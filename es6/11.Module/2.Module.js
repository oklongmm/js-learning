// ES6 模块与 CommonJS 模块的差异
// 1.CommonJS 模块输出的是一个值的拷贝，ES6 模块输出的是值的引用。
// 2.CommonJS 模块是运行时加载，ES6 模块是编译时输出接口。

// CommonJS 模块输出的是值的拷贝，也就是说，一旦输出一个值，模块内部的变化就影响不到这个值。

// lib.js
var counter = 3;
function incCounter () {
    counter++;
}
module.exports = {
    counter: counter,
    incCounter: incCounter,
};

// main.js
var mod = require('./lib');

console.log(mod.counter);  // 3
mod.incCounter();
console.log(mod.counter); // 3

// 这是因为mod.counter是一个原始类型的值，会被缓存。除非写成一个函数，才能得到内部变动后的值。

// -------------------------------------------------------------

// ES6 模块是动态引用，并且不会缓存值，模块里面的变量绑定其所在的模块。

// lib.js
export let counter = 3;
export function incCounter() {
    counter++;
}

// main.js
import { counter, incCounter } from './lib';
console.log(counter); // 3
incCounter();
console.log(counter); // 4

// ES6 模块不会缓存运行结果，而是动态地去被加载的模块取值，并且变量总是绑定其所在的模块