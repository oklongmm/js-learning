// 第一组
export default function foo() { // 输出
    // ...
}

import foo from 'foo';  // 输入

// 第二组
export function foo() {
    // ...
}

import {foo} from 'foo';

// export default命令用于指定模块的默认输出。
// 显然，一个模块只能有一个默认输出，因此export default命令只能使用一次。
// 所以，import命令后面才不用加大括号，因为只可能唯一对应export default命令。